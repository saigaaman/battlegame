﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class RaceMainCamera : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] GameObject player;
    RaceStatus raceStatus;
    [SerializeField] CinemachineVirtualCamera _vCam;
    CinemachinePOV pov;

    //POVを最優先に認識させるため、Awakeを使う。これによりOnEnable内POVが開幕も問題無く動く
    void Awake()
    {
        pov = _vCam.GetCinemachineComponent<CinemachinePOV>();
        raceStatus = player.GetComponent<RaceStatus>();
    }

    //enabledがオンになる度に初期値へリセット
    private void OnEnable()
    {
        pov.m_HorizontalAxis.Value = 0;
        pov.m_VerticalAxis.Value = 0;
    }




    //マウスクリック時だけ一定のスピードでスクロールを許す。
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
 
                pov.m_HorizontalAxis.m_MaxSpeed = 200f;
                //もしマウスを横移動していれば
                if(Input.GetAxis("Mouse X") != 0) {
                    //左右方向移動時のみ、減速させる
                    if (raceStatus.MaxSpeed >= 3.0f)
                    {
                        //0.5加速に対し1.0減速
                        raceStatus.MoveSpeed -= 1.0f * Time.deltaTime;
                    }
                }
                //横移動していなければ減速させる
                else
                {

                }
                pov.m_VerticalAxis.m_MaxSpeed = 200f;
        }
        else
        {
            pov.m_HorizontalAxis.m_MaxSpeed = 0f;
            pov.m_VerticalAxis.m_MaxSpeed = 0f;
          

        }



    }






}
