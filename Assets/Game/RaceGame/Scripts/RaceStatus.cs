﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceStatus : MonoBehaviour
{
#pragma warning disable 0649
    //プレイヤーのステータスでゲームの操作など管理する

    private enum StateEnum
    {
        Normal,//ノーマル状態
        Out,//コースアウト中
        AirWalk,//空中
        Stop//強制停止中

    }

    //移動可能かどうか
    public bool IsNormal => StateEnum.Normal == state;
    public bool IsOut=> StateEnum.Out == state;
    public bool IsAirWalk => StateEnum.AirWalk == state;
    public bool IsStop => StateEnum.Stop == state;

    private StateEnum state = StateEnum.Normal;


    private float moveSpeed = 0.0f;//ユニティちゃんの移動スピードの値。最初は１からどんどん加速
    //moveSpeedのプロパティ。外部アクセス時　参照；MoveSpeed()　上書きMoveSpeed(値)
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }

    private float maxSpeed = 10.0f;//最大加速。地形により上下する事あり。基本値は10

    public float MaxSpeed
    {
        get
        {
            return maxSpeed;
        }
        set
        {
            maxSpeed = value;
        }

    }



    //走っているかどうかを判定する
    private bool run;
    public bool Run
    {
        get
        {
            return run;
        }
        set
        {
            run = value;
        }
    }

    //数値や変数はプロパティから変更。

    //状態変化はメソッドで変更する。




    //コースアウトした場合
    public void CourseOut()
    {
        state = StateEnum.Out;
    }

    //空中状態
    public void AirUp()
    {
        state = StateEnum.AirWalk;
    }

    //停止状態（スタート前など、何らかの状況）
    public void Stop()
    {
        state = StateEnum.Stop;
    }

    public void NormalGoState()
    {
        state = StateEnum.Normal;
    }


}
