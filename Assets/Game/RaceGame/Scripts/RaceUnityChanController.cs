﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class RaceUnityChanController : MonoBehaviour
{
#pragma warning disable 0649
    CharacterController controller;
    Animator animator;
    RaceStatus raceStatus;
    [SerializeField] GameObject player;
    private Vector3 direction;//ユニティちゃんの移動方向 
    private Vector3 velocity;//ユニティちゃんの実際の移動距離
    private Vector3 moveDirection;//ユニティちゃんの実際の移動
    private Vector3 jumpVelocity;//ユニティちゃんのジャンプ力









    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();//ユニティちゃんを初期位置へ配置し、キャラコンをオン
        raceStatus = GetComponent<RaceStatus>();//プレイヤーの状態を受け取るためのもの


    }


    void Update()
    {
        //地上なら
        if (controller.isGrounded == true)
        {
            ///通常加速操作
            //アクセルを押すとスピードが上がる
            if (Input.GetKey(KeyCode.Z))
            {
                //走り出しのスピードは3
                if (raceStatus.Run == false)
                {
                    raceStatus.MoveSpeed = 3f;
                }

                raceStatus.MoveSpeed += 0.5f * Time.deltaTime;

            }
            //ジャンプボタンを押すとジャンプする
            if (Input.GetKeyDown(KeyCode.X))
            {
                //ジャンプ数値をセット
                jumpVelocity.y = 4f;
                Debug.Log("ジャンプ");
                animator.SetTrigger("Jump");
            }
            //ブレーキをかけると急激にスピードが下がる
            if (Input.GetKey(KeyCode.C))
            {

                raceStatus.MoveSpeed -= 3f * Time.deltaTime;

            }


            //速度が最大値になったら
            if (raceStatus.MoveSpeed > raceStatus.MaxSpeed)
            {
                raceStatus.MoveSpeed = raceStatus.MaxSpeed;
                
            }
            //速度が最低値の０になったら
            else if (raceStatus.MoveSpeed <= 0)
            {
                raceStatus.Run = false;
                //0固定にする
                raceStatus.MoveSpeed = 0;
            }
        //空中にいるなら
        }
        else
        {
            if (raceStatus.MoveSpeed > 0)
            {
                //もし空中にいるなら加速やブレーキが利かず、エンジンブレーキ的なものが微妙に効く
                raceStatus.MoveSpeed -= 0.5f * Time.deltaTime;
            }
            //animator.SetBool("Grounded",false);//アニメーションに空中滑空状態を送る

            jumpVelocity.y += Physics.gravity.y* Time.deltaTime;

        }
        //左右キーでカメラの方向を曲げる
        moveDirection = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;//froward変数にメインカメラの左右値を基準とし、それを正規化する（x,z座標のみ）
        moveDirection *= raceStatus.MoveSpeed;//移動値にスピードをかける
        moveDirection.y = jumpVelocity.y;//その後、ジャンプ数値を加える

        //移動方向にキャラを向けるようにする(Vector3を使用し、ポジションのxとyを合わせる)
        transform.LookAt(transform.position + new Vector3(moveDirection.x, 0, moveDirection.z));


        //最後にキャラクターオブジェクトを動かし、位置を確定（ちょうど１フレーム分）
        controller.Move(moveDirection* Time.deltaTime);

        if (raceStatus.MoveSpeed > 0)
        {
            raceStatus.Run = true;
            //アニメ側にスピードを伝える
            animator.SetFloat("Speed", raceStatus.MoveSpeed * 0.2f);
        }

        //着地してるか否かは常に共有
        animator.SetBool("Grounded", controller.isGrounded);
        //走ってるか否かは常にアニメ側と共有
        animator.SetBool("Run", raceStatus.Run);

        


    }




}