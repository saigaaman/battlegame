﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStatus : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] GameObject player;//走るプレイヤー
    [SerializeField] Text speedValue;//速度表記

    [SerializeField] Text time;//UIのTIME表記
    private float realTime;//総時間
    private string minutes;//分
    private string seconds;//秒

    RaceStatus raceStatus;


    void Start()
    {
        raceStatus = player.GetComponent<RaceStatus>();
    }

    // Update is called once per frame
    void Update()
    {
        speedValue.text = "";
        if (raceStatus.MoveSpeed<10)
        {
            speedValue.text +="0";
        }

        //速度をテキストに反映
        speedValue.text += ((int)raceStatus.MoveSpeed).ToString() + "/" + ((int)raceStatus.MaxSpeed).ToString();

        //Timeテキストは毎回リセット
        seconds = "";
        minutes = "";

        //１秒基準のリアルタイムを変数と共有
        realTime+=1.0f*Time.deltaTime;
        //秒数を60で割り、余りが10に満たない場合はsecondsの最初に0を入れる
        if(realTime%60<10){
                seconds = "0";
        }
        //60割った余りを小数点二桁表記し、間のピリオドをコロンに変換
        seconds += (realTime % 60).ToString("F2").Replace(".", ":");
        //60割った数を表記
        minutes += ((int)(realTime / 60)).ToString() + ":";

        //時間をテキストに反映
        time.text ="TIME  " +minutes+seconds;

    }
}
