﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviour
{
    public GameObject player;
    PlayerStatus playerStatus;

    public Text HpValue;
    public Text MpValue;
    public Text SpValue;


    void Start()
    {
        playerStatus = player.GetComponent<PlayerStatus>();   
    }

    void Update()
    {
        HpValue.text =((int)playerStatus.Hp).ToString() +"/"+ ((int)playerStatus.MaxHp).ToString();
        MpValue.text =((int)playerStatus.Mp).ToString() + "/" + ((int)playerStatus.MaxMp).ToString();
        SpValue.text =((int)playerStatus.Sp).ToString() + "/" + ((int)playerStatus.MaxSp).ToString();

    }
}
