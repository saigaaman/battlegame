﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoundScript : MonoBehaviour
{
    //オーディオソースを統括する配列
    AudioSource[] sounds;


    //基本的にプレイヤー
    ///////sound////////////////// 
    ///サウンド
    ///
    //攻撃のモーション時のサウンド
    public AudioClip[] swingSound = new AudioClip[0];

    //ダメージを受ける際の音。クリかどうかで分けるのもあり
    public AudioClip[] hitSound = new AudioClip[0];

    //ダメージを受ける際の音。クリかどうかで分けるのもあり
    public AudioClip[] damageSound = new AudioClip[0];



    //死んだ時のサウンド
    public AudioClip[] destroySound = new AudioClip[0];





    // Use this for initialization
    void Start()
    {
        //0 Sound
        sounds = GetComponents<AudioSource>();


    }

    void Update()
    {

        //sounds[0].PlayOneShot(attackVoice[0]);

    }



    public void PlaySwingSound(int value)
    {
        //引数に入れられたものにより、違う音を出す
        sounds[0].PlayOneShot(swingSound[value]);
    }

    public void PlayHitSound(int value)
    {
        //ヒットする際の音
        sounds[0].PlayOneShot(hitSound[value]);
    }





    public void PlayDamageSound(int value)
    {
        //引数に入れられたものにより、違う音を出す
        sounds[0].PlayOneShot(damageSound[value]);
    }

    public void PlayDestroySound(int value)
    {
        //引数に入れられたものにより、違う音を出す
        sounds[0].PlayOneShot(destroySound[value]);
    }










}
