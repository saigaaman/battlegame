﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundsScript : MonoBehaviour
{
    //オーディオソースを統括する配列
    AudioSource[] sounds;

    ///////voice////////////////// 
    //0~3は通常攻撃ボイス
    public AudioClip[] attackVoice = new AudioClip[0];

    //ジャンプ時のボイス
    public AudioClip[] jumpVoice = new AudioClip[0];

    //被ダメージ時のボイス
    public AudioClip[] damageVoice = new AudioClip[0];

    //死亡時のボイス
    public AudioClip[] dieVoice = new AudioClip[0];

    ///////sound////////////////// 
    ///サウンド
    ///
    //剣をブンブン振る音はクリティカル時とそうでないかの2種にする
    public AudioClip[] twoHandsSwingSound = new AudioClip[0];

    //同様に、命中時の音も2種
    public AudioClip[] twoHandsHitSound = new AudioClip[0];

    //ガードした時のサウンド
    public AudioClip[] guardSound = new AudioClip[0];

    //足音のサウンド。１種類のみだが、追加する可能性あるので配列にしている
    public AudioClip[]  footStepSound = new AudioClip[0];



    // Use this for initialization
    void Start()
    {
        //0 Voice
        //1 Sound
        //2 music
        sounds = GetComponents<AudioSource>();


    }

    void Update()
    {

            //sounds[0].PlayOneShot(attackVoice[0]);


    }

    public void PlayNormalAttackVoice()
    {
        //入っている要素数でシャッフルし、ランダムのボイスを鳴らす
        sounds[0].PlayOneShot(attackVoice[Random.Range(0, attackVoice.Length)]);
    }

    public void PlayJumpVoice()
    {
        //入っている要素数でシャッフルし、ランダムのボイスを鳴らす
        sounds[0].PlayOneShot(jumpVoice[Random.Range(0, jumpVoice.Length)]);
    }


    public void PlayDamageVoice()
    {
        //入っている要素数でシャッフルし、ランダムのボイスを鳴らす
        sounds[0].PlayOneShot(damageVoice[Random.Range(0, damageVoice.Length)]);
    }

    public void PlayDieVoice()
    {
        // 入っている要素数でシャッフルし、ランダムのボイスを鳴らす
         sounds[0].PlayOneShot(dieVoice[Random.Range(0, dieVoice.Length)]);
    }





    public void PlaySwingSound(int value)
    {
        //引数に入れられたものにより、違う音を出す
        sounds[1].PlayOneShot(twoHandsSwingSound[value]);
    }


    public void PlayHitSound(int value)
    {
        //引数に入れられたものにより、違う音を出す
        sounds[1].PlayOneShot(twoHandsHitSound[value]);
    }

    public void PlayGuardSound(int value)
    {
        //引数に入れられたものにより、違う音を出す
        sounds[1].PlayOneShot(guardSound[value]);
    }


    public void PlayFootStepSound()
    {
        //引数に入れられたものにより、違う音を出す
        sounds[1].PlayOneShot(footStepSound[0]);
    }












}
