﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MobStatus
{



    //移動速度。ダッシュ攻撃、攻撃中のモーション移動にも影響
    private float moveSpeed=7.0f;
    //moveSpeedのプロパティ。外部アクセス時　参照；MoveSpeed()　上書きMoveSpeed(値)
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }

    }


    PlayerSoundsScript soundScript;
    TwoHandsAttack twoHandsAttack;



    //一定時間ごとにHPとMPを回復させる為の変数
    private float recoveryTime;//治療させる時間
    private float recoveryHpValue=0.05f;//回復数値
    private float recoveryMpValue = 0.1f;//回復数値




    //一定時間ごとにHPとMPは回復。アーマー値と違い、死んでいなければいつでも回復
    public void HpMpCharge()
    {
        if (state != StateEnum.Die) return;
        recoveryTime += 1 * Time.deltaTime;
        if (recoveryTime>=20)
        {
            hp += maxHp * recoveryHpValue;
            mp += maxMp * recoveryMpValue;
            if (hp>=maxHp)hp = maxHp;
            if (mp >= maxMp)mp += maxMp;
            recoveryTime = 0;
        }

    }




    protected override void Start()
    {

        base.Start();
        maxHp = 1500f;
        maxMp = 750f;
        maxSp = 200f;
        hp = MaxHp;
        mp = MaxMp;
        sp = MaxSp;
        soundScript = GetComponent<PlayerSoundsScript>();
        twoHandsAttack = GetComponent<TwoHandsAttack>();
    }

    private void Update()
    {

        //自然回復の判定を繰り返す
        HpMpCharge();
    }


    //指定したダメージを受ける
    public override void Damage(int damage)
    {
        //もしステータスがDieもしくはSpアタック中なら何もしない
        if (state == StateEnum.Die || state == StateEnum.SpAttack) return;
        //攻撃中なら攻撃中断させるべくコライダーを消す
        twoHandsAttack.AttackColliderOff();


            //被ダメボイス
        soundScript.PlayDamageVoice();
            //死んでいなければ
            if (hp > 0)
            {
                animator.SetTrigger("Damage");//ダメージモーションになる
            }
        
        //ライフにダメージを反映
        hp -= damage;
        //HPが残ってたらここで処理は終了
        if (hp > 0) return;

            //0になっている場合は以下の処理
            //ステータスをDieに、Dieアニメーションをトリガーに、Die処理を
            state = StateEnum.Die;
            Debug.Log("Die");
            animator.SetTrigger("Die");
            soundScript.PlayDieVoice();
            OnDie();
    }



    protected override void OnDie()
    {


    }




}
