﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerCamera : MonoBehaviour
{
#pragma warning disable 0649

    [SerializeField] CinemachineFreeLook freeLookCamera;
    public GameObject player;
    LockOn lockOn;

    private void Start()
    {
        lockOn = player.GetComponent<LockOn>();
    }





    // Update is called once per frame    
    void Update()
    {
        if (freeLookCamera.enabled == true)
        {
            //もしターゲットをロックしてるもしくはクリックしていなければ
            if (lockOn.targetLock == true|| Input.GetButton("Fire1")==false)
            {
                freeLookCamera.m_XAxis.m_MaxSpeed = 0;
                freeLookCamera.m_YAxis.m_MaxSpeed = 0;
            }
            //ロックしておらず、クリックした状態なら
            else if (Input.GetButton("Fire1"))
            {
                freeLookCamera.m_XAxis.m_MaxSpeed = 300;
                freeLookCamera.m_YAxis.m_MaxSpeed = 2;
            }

            float scroll = Input.GetAxis("Mouse ScrollWheel");
            freeLookCamera.m_Orbits[0].m_Radius -= scroll * 5;
            freeLookCamera.m_Orbits[1].m_Radius -= scroll * 5;
            freeLookCamera.m_Orbits[2].m_Radius -= scroll * 5;
            freeLookCamera.m_Orbits[0].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[0].m_Radius, 2, 8);
            freeLookCamera.m_Orbits[1].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[1].m_Radius, 2, 10);
            freeLookCamera.m_Orbits[2].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[2].m_Radius, 1, 6);
        }
    }
    public void ZoomIn()
    {
        if (freeLookCamera.enabled == true)
        {
            freeLookCamera.m_Orbits[0].m_Radius -= 0.5f;
            freeLookCamera.m_Orbits[1].m_Radius -= 0.5f;
            freeLookCamera.m_Orbits[2].m_Radius -= 0.5f;
            freeLookCamera.m_Orbits[0].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[0].m_Radius, 2, 8);
            freeLookCamera.m_Orbits[1].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[1].m_Radius, 2, 10);
            freeLookCamera.m_Orbits[2].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[2].m_Radius, 1, 6);
        }
    }
    public void ZoomOut()
    {
        if (freeLookCamera.enabled == true)
        {
            freeLookCamera.m_Orbits[0].m_Radius += 0.5f;
            freeLookCamera.m_Orbits[1].m_Radius += 0.5f;
            freeLookCamera.m_Orbits[2].m_Radius += 0.5f;
            freeLookCamera.m_Orbits[0].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[0].m_Radius, 2, 8);
            freeLookCamera.m_Orbits[1].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[1].m_Radius, 2, 10);
            freeLookCamera.m_Orbits[2].m_Radius = Mathf.Clamp(freeLookCamera.m_Orbits[2].m_Radius, 1, 6);
        }
    }

    //引数のキャラクター角度＆カメラ角度に応じて最適なカメラ回転を与えるメソッド
    public void CamRota(float charaRota,float cameraRota)
    {

    }











}
