﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class LockOn : MonoBehaviour
{
    //ロックオン対象
    GameObject target = null;
    //ロックオン対象のゲッター
    public GameObject Target => target;


    public CinemachineFreeLook freeLookCamera;
    public bool targetLock;
    private float playerYaxis;//プレイヤーの向いている方向。カメラ方向を正面に合わせる為に使う
    Animator animator;
    PlayerStatus playerStatus;
    MobStatus mobStatus;
    HitCounter hitCounter;

    //ターゲットに設定するゲームオブジェクトを格納する配列
    GameObject[] gos;

    private void Start()
    {
        animator = GetComponent<Animator>();
        playerStatus=GetComponent<PlayerStatus>();
        hitCounter = GetComponent<HitCounter>();
    }


    private void Update()
    {
        
        //もしＬキーが押されたら
        if (Input.GetKeyDown(KeyCode.L))
        {
            //既にロックオン中なら解除
            if (target != null)
            {
                //ロック解除
                target = null;
                targetLock = false;
                animator.SetBool("LockOn", targetLock);

                //ロックオン中でなければロックオン
            }
            else
            {
                //ターゲットを見つけ出し、ロックする
                target=FindClosestEnemy();
                if (target!=null) {
                    targetLock = true;
                    animator.SetBool("LockOn", targetLock);
                    mobStatus = target.GetComponent<MobStatus>();//ターゲットの状態を取得
                }
            }

        }
        //もしロックオン中であり、かつRが押された
        else if (Input.GetKeyDown(KeyCode.R)&& targetLock==true)
        {
            if (playerStatus.IsGoAttack) return;//もし攻撃に行ってる最中ならここで弾く

            //※もし今後エラーが出たらFindClosestEnemy();を削除すれば元通り。多分大丈夫
            //ターゲットをそのままにいったん敵の情報を取得（敵が増減してる場合があるので）
            FindClosestEnemy();
            //他のターゲットへ切り替えてロック
            EnemySearch();
            mobStatus = target.GetComponent<MobStatus>();//ターゲットの状態を取得

        }



        if (targetLock == true)
        {
            //ターゲットが居て、かつ死んでいない場合
            if (target != null&&mobStatus.IsDie==false)
            {
                //ロック中はターゲットの方向をスムーズに向く
                //transform.LookAt(target.transform);
                Quaternion targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);


                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 10);

                //現在のプレイヤーの角度を取得
                playerYaxis = this.transform.localEulerAngles.y;

                //カメラをプレイヤー方向へ素早く動かす         
                freeLookCamera.m_XAxis.Value =playerYaxis;
                //Yaxisは初期設定にする
                freeLookCamera.m_YAxis.Value = 0.5f;

                //距離が離れたらロックオンを解除する
                if (Vector3.Distance(target.transform.position, transform.position) > 30)
                {
                    target = null;
                
                }



            //ターゲットが死んだ場合
            }else if (target != null && mobStatus.IsDie == true)
            {
                //コンボ時間を延長
                hitCounter.ComboPlus();
                //近くのターゲットを見つけ出し、ロックする
                target = FindClosestEnemy();
                //もしターゲットがいればステータスも取得する
                if (target!=null) {
                    mobStatus = target.GetComponent<MobStatus>();//ターゲットの状態を取得
                }
            }
            else
            {
                //ターゲットがいないならロック解除
                targetLock = false;
                animator.SetBool("LockOn", targetLock);
            }
            //流れ→ターゲットが死んだとして、死んだ場合の部分で弾かれるのでRボタンでターゲット切り替えても死んだターゲットは一切ロック対象にならない
            //敵が死ぬと敵側のタグは敵が死んだ状態のタグに切り替わる


        }
        
    }   

    private GameObject FindClosestEnemy()
    {
        //ターゲットを格納するオブジェクト
        gos = GameObject.FindGameObjectsWithTag("Enemy");


        //ターゲットに決めたオブジェクト
        GameObject closest = null;
        //ターゲットと自分との距離の現在最小距離
        float distance = Mathf.Infinity;
        //自身のポジション
        Vector3 position = transform.position;

        //格納された
        foreach (GameObject go in gos)
        {

            //ターゲットと自分とのベクトル差を算出し代入
            Vector3 diff = go.transform.position-position;
            //ベクトルを二乗して距離を算出
            float curDistance = diff.sqrMagnitude;
            //もし該当ターゲットとの距離が近ければターゲットと最小距離を更新
            if (curDistance<distance)
            {
                closest = go;
                distance = curDistance;
            }

        }
        if (closest!=null)
        {
            //一番近くの敵が範囲外ならロックオンしない
            if (Vector3.Distance(closest.transform.position, transform.position) > 30)
            {
                closest = null;
            }
        }

        return closest;
    }



    //他のターゲットを探すメソッド
    private void EnemySearch()
    {

        //targetをgos配列内で検索し、配列の最後尾にいたら最前列を入れる、他は配列+1
        for (int i = 0; i < gos.Length - 1; i++)
        {

            if (target == gos[i])
            {
                target = gos[i + 1];
                return;
            }
            else if (target == gos[gos.Length - 1])
            {
                target = gos[0];
                return;
            }
        }

    }





}
