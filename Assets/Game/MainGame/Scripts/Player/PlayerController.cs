﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    CharacterController controller;
    PlayerStatus playerStatus;
    Animator animator;
    PlayerSoundsScript soundScript;
    LockOn lockOn;

    private bool wellGetThere=false;//ダッシュ攻撃のターゲットに到着しているか否か

    private Vector3 moveDirection;//ユニティちゃんの実際の移動
    private Vector3 jumpVelocity;//ユニティちゃんのジャンプ力


    void Start()
    {
        soundScript = GetComponent<PlayerSoundsScript>();
        animator = GetComponent<Animator>();//アニメーションを取得
        controller = GetComponent<CharacterController>();//キャラコンを取得
        //GameObject player = GameObject.Find("TwohandsUnitychan");//必要なら使うオブジェクト検索
        playerStatus = GetComponent<PlayerStatus>();
        lockOn = GetComponent<LockOn>();


    }


    void Update()
    {
        //死んでるか、ｸｰﾙﾀﾞｳﾝ中は操作を一切受け付けない
        if (playerStatus.IsDie||playerStatus.IsCoolDown) return;
        //地上なら
        if (controller.isGrounded == true)
        {
            //もしダッシュ攻撃中なら一切の操作移動を受け付けない
            if (playerStatus.IsGoAttack)
            {
                playerStatus.MoveSpeed = 30.0f;
                //ロックオン対象
                Vector3 attackTarget = lockOn.Target.transform.position;
                moveDirection = (attackTarget - transform.position);

                if (Vector3.Distance(transform.position, attackTarget) < 1.5f) {
                    animator.SetTrigger("WellGetThere");
                    moveDirection.x = 0;
                    moveDirection.z = 0;
                    playerStatus.StateNormalPossible();
                    wellGetThere = true;
                }



            }
            //ダッシュ攻撃中でなければ移動を受け付ける
            else {
                if (playerStatus.IsNormal || playerStatus.IsRun)
                {
                    playerStatus.MoveSpeed = 7.0f;
                    //移動計算

                    //前後キーが押されたらfroward変数にメインカメラの左右値を入力（x,z座標のみ）
                    Vector3 forward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1));
                    //もし左右いずれかのキーが押されたらforward とrightに値を入れる
                    Vector3 right = Vector3.Scale(Camera.main.transform.right, new Vector3(1, 0, 1));
                    //その移動方向情報をmoveDirectionに渡し、キャラのスピード値をかける
                    moveDirection = Input.GetAxis("Horizontal") * right + Input.GetAxis("Vertical") * forward;

                    //ターゲットがロックされてない状態でなければ毎フレームこちらで方向を向ける
                    if (lockOn.targetLock != true)
                    {
                        //移動方向にキャラを向けるようにする(Vector3を使用し、ポジションのxとyを合わせる)
                        transform.LookAt(transform.position + new Vector3(moveDirection.x, 0, moveDirection.z));
                    }
                } else {
                    moveDirection.x = 0;
                    moveDirection.z = 0;
                }


            }


            //移動計算ここまで


            //アクションコマンドここから
            //地上アクションボタンを受け付け
            //もし何かしらのキーが押されていれば
            if (Input.anyKeyDown)
            {

                switch (Input.inputString)
                {
                    //ジャンプボタンが押されているなら
                    case "x":
                        //もし移動攻撃中なら操作受け付けない
                        if (playerStatus.IsGoAttack) return;

                        //ジャンプ数値をセット
                        jumpVelocity.y = 6f;
                        Debug.Log("ジャンプ");
                        animator.SetTrigger("Jump");
                        soundScript.PlayJumpVoice();
                        break;

                    //通常攻撃ボタンが押されているなら
                    case "q":
                        //ステータスが入力済状態もしくは攻撃移動中なら処理ここで終了
                        if (playerStatus.IsInput || playerStatus.IsGoAttack) return;


                        //ターゲットをロックオンしてる、かつ至近距離でなく、かつダッシュ攻撃が届いていない状態なら、ダッシュ攻撃状態へ移行
                        if (lockOn.targetLock)
                        {
                            //ターゲットが0.3以上高い場合もしくは1.0以上も低い位置にいる場合は発動できない
                            if (transform.position.y - lockOn.Target.transform.position.y <= -0.3 && transform.position.y - lockOn.Target.transform.position.y >= 1.0) return;

                            //もし既に至近距離なら
                            if (Vector3.Distance(transform.position, lockOn.Target.transform.position) < 1.5f)
                            {
                                animator.SetTrigger("Attack");
                                if (playerStatus.IsAttack)
                                {
                                    //攻撃中ならインプット状態にして入力をこれ以上受付できなくする
                                    playerStatus.StateInputPossible();
                                }
                            }
                            //至近距離でなければダッシュ攻撃中へ移行
                            else
                            {
                                //ノーマル状態以外は受け付けない
                                if (playerStatus.IsNormal == false) return;
                                wellGetThere = false;
                                //アニメ側に情報を送り、ステータスをダッシュ攻撃中へ
                                animator.SetTrigger("AttackMove");
                                playerStatus.StateGoAttackPossible();
                            }
                        }
                        //ロックオンしていないなら通常へ
                        else {
                            animator.SetTrigger("Attack");
                            if (playerStatus.IsAttack)
                            {
                                //攻撃中ならインプット状態にして入力をこれ以上受付できなくする
                                playerStatus.StateInputPossible();
                            }
                        }

                        break;

                    //特殊攻撃ボタンが押されているなら
                    case "e":
                        //もし移動攻撃中なら操作受け付けない
                        if (playerStatus.IsGoAttack) return;

                        break;

                    //必殺技ボタンが押されているなら
                    case "z":

                        break;

                    //もしガードボタンが押されているなら
                    case "g":
                        //もし移動攻撃中なら操作受け付けない
                        if (playerStatus.IsGoAttack) return;
                        break;


                    //何も押されていなければ
                    default:

                        break;


                }
            }

        }
        //空中での処理
        else
        {
            //animator.SetBool("Grounded",false);//アニメーションに空中滑空状態を送る（必要になった場合）

            //カメラから見たZ座標方向を設定
            Vector3 forward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1));
            //カメラから見たX座標方向を設定
            Vector3 right = Vector3.Scale(Camera.main.transform.right, new Vector3(1, 0, 1));

            //その移動方向情報に対し入力キーによる微妙な滑空を効かせる
            moveDirection += (Input.GetAxis("Horizontal") * right) * 0.2f + (Input.GetAxis("Vertical") * forward) * 0.2f;
            //ロックオン時のみ、空中方向転換をしない
            if (lockOn.targetLock!=true) {
                //移動方向にキャラを向けるようにする(Vector3を使用し、ポジションのxとyを合わせる)
                transform.LookAt(transform.position + new Vector3(moveDirection.x, 0, moveDirection.z));
            }
            //空中時のみ、重力をかける
            jumpVelocity.y += Physics.gravity.y * Time.deltaTime;



        }

        moveDirection= moveDirection.normalized;
        moveDirection *= playerStatus.MoveSpeed;//移動値にスピードをかける
        moveDirection.y = jumpVelocity.y;//その後、ジャンプ数値を加える





        //移動値によりアニメーションを送る
        if (moveDirection.z != 0 || moveDirection.x != 0)
        {
            //アニメ側に移動速度を送る
            animator.SetFloat("Speed", playerStatus.MoveSpeed * 0.2f);
        }
        else
        {
            animator.SetFloat("Speed", playerStatus.MoveSpeed * 0.0f);
        }


        //最後にキャラクターオブジェクトを動かし、位置を確定（ちょうど１フレーム分）
        controller.Move(moveDirection * Time.deltaTime);

        animator.SetBool("Grounded", controller.isGrounded);

    }

    //ノックバックメソッド。移動処理はキャラコン側で行う
    private void KnockBack(int Value)
    {


    }











    /*
     メモ






     */
}
