﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitCounter : MonoBehaviour
{
    private float hitCounter;//攻撃がヒットしてる回数
    private float damageCounter;//ダメージの総計
    private bool comboStart;//コンボが始まったか否か
    public bool ComboStart
    {
        get
        {
            return comboStart;
        }
    }

    private float comboBonus;//コンボが始まったか否か
    public float ComboBonus
    {
        get
        {
            return comboBonus;
        }
        set
        {
            comboBonus = value;
        }

    }

    private float comboTimer;//コンボが続く時間
    public Text damageText;//総ダメージ表記
    public Text comboText;//コンボ数表記
    public Text bonusText;//コンボボーナス表記
    public Text statusText;//コンボの状態（英字）エクセレント等









    // Update is called once per frame
    void Update()
    {
        //コンボ開始していたらカウント開始
        if (comboStart == true)
        {
            comboTimer += 1 * Time.deltaTime;
            damageText.text = damageCounter.ToString() + "Damage";
            comboText.text = hitCounter.ToString() + "Hits!!";
        }
        //もしヒットしないまま1秒経過したらコンボ終了（ここではスコアはリセットしない）
        if (comboTimer>=1)
        {
            comboTimer = 0;
            comboStart = false;
        }
        
    }

    //ヒットが続けばコンボタイマーをリセットする
    public void ComboContinue(int damagePoint)
    {
        comboTimer = 0;
        hitCounter++;
        damageCounter += damagePoint;

        //コンボボーナス倍数と状態表記
        //5HITより低ければ
        if (hitCounter>= 1 && hitCounter < 5)
        {
            comboBonus = 1.0f;
            bonusText.text = "ComboBonus x1.0";
            statusText.text = "Chain!";
        }
        else if (hitCounter >= 5 && hitCounter < 10)
        {
            comboBonus = 1.1f;
            bonusText.text = "ComboBonus x1.1";
            statusText.text = "Great!!";
        }
        else if (hitCounter >= 10 && hitCounter < 15)
        {
            comboBonus = 1.25f;
            bonusText.text = "ComboBonus x1.25";
            statusText.text = "Super!!";
        }
        else if (hitCounter >= 15 && hitCounter < 20)
        {
            comboBonus = 1.5f;
            bonusText.text = "ComboBonus x1.5";
            statusText.text = "excellent!!";
        }
        else if (hitCounter >= 20)
        {
            comboBonus = 1.8f;
            bonusText.text = "ComboBonus x1.8";
            statusText.text = "wonderful!!";
        }
        else
        {
            comboBonus = 1.0f;
            bonusText.text = "ComboBonus x1.0";
            statusText.text = "CoolDown";
        }

    }

    //ダメージ食らうか、初撃のみこれを実行させる
    public void ComboStartUp()
    {
        comboStart = true;
        comboBonus = 1.0f;
        damageCounter = 0;
        hitCounter = 0;
    }

    //ターゲットを倒すとボーナスとして3秒の間コンボ時間が延びる
    public void ComboPlus()
    {
        comboTimer -= 3;
    }



}
