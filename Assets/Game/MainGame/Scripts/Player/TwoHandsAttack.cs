﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerStatus))]
public class TwoHandsAttack : MonoBehaviour
{
#pragma warning disable 0649
    private PlayerStatus status;
    private int damagePoint;
    //private int ran;//乱数用パラメータ
    Animator animator;
    PlayerSoundsScript soundScript;//サウンド
    HitCounter hitCounter;

    public Collider attack1Collider;
    public Collider attack2Collider;
    public Collider attack3Collider;
    public Collider attack4Collider;


    private void Start()
    {
        soundScript = GetComponent<PlayerSoundsScript>();
        animator = GetComponentInChildren<Animator>();
        status = GetComponent<PlayerStatus>();
        hitCounter = GetComponent<HitCounter>();
    }


    public void OnHitAttack(Collider collider)
    {
        //ターゲット範囲にいるモブステータスを取得
        var targetMob = collider.GetComponent<MobStatus>();
        //範囲内座標にターゲットモブがいなければ何もしない
        if (null == targetMob) return;
        //ランダム変数を適用し、プレイヤーにダメージを与える
        //ダメージポイントを0.8以上1.2未満の中から乱数作り適用
        damagePoint = (int)(status.Power * (Random.Range(0.8f, 1.2f)));
        //できたダメージポイントを表記
        if (!status.IsSpAttack)
        {
            //status.SpUp();
        }
        //コンボスタートされてなければ初回だけコンボをリセット
        if (hitCounter.ComboStart==false)
        {
            hitCounter.ComboStartUp();
        }
        //Hitとダメージ加算メソッドの実行
        hitCounter.ComboContinue(damagePoint);
        //最後にHITボーナスを乗算する
        damagePoint = (int)(damagePoint*hitCounter.ComboBonus);
        //数値をターゲットへ渡す
        targetMob.Damage(damagePoint);
        //当たった音を響かせる
        soundScript.PlayHitSound(0);
    }



    //攻撃の開始時にモーションから呼ばせる
    public void OnAttack1Start()
    {
        soundScript.PlayNormalAttackVoice();
        soundScript.PlaySwingSound(0);
        damagePoint = (int)(status.Power * 1.0f);
        attack1Collider.enabled = true;

    }

    //攻撃の終了時に呼ばれる
    public void OnAttack1Finished()
    {
        attack1Collider.enabled = false;
    }

    //攻撃の開始時にモーションから呼ばせる
    public void OnAttack2Start()
    {
        soundScript.PlayNormalAttackVoice();
        soundScript.PlaySwingSound(0);
        damagePoint = (int)(status.Power * 1.3f);
        attack2Collider.enabled = true;
    }

    //攻撃の終了時に呼ばれる
    public void OnAttack2Finished()
    {
        attack2Collider.enabled = false;
    }

    //攻撃の開始時にモーションから呼ばせる
    public void OnAttack3Start()
    {
        soundScript.PlayNormalAttackVoice();
        soundScript.PlaySwingSound(0);
        damagePoint = (int)(status.Power * 1.35f);
        attack3Collider.enabled = true;
    }

    //攻撃の終了時に呼ばれる
    public void OnAttack3Finished()
    {
        attack3Collider.enabled = false;
    }

    //攻撃の開始時にモーションから呼ばせる
    public void OnAttack4Start()
    {
        soundScript.PlayNormalAttackVoice();
        soundScript.PlaySwingSound(0);
        damagePoint = (int)(status.Power * 1.5f);
        attack4Collider.enabled = true;
    }

    //攻撃の終了時に呼ばれる
    public void OnAttack4Finished()
    {
        attack4Collider.enabled = false;
    }


    //ダメージを受けた際などに使う攻撃コライダーオフメソッド
    public void AttackColliderOff()
    {
        OnAttack1Finished();
        OnAttack2Finished();
        OnAttack3Finished();
        OnAttack4Finished();

    }



}
