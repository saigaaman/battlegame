﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EnemyMove : MonoBehaviour
{
#pragma warning disable 0649

    //移動に使うもの
    private bool targetLock;//ターゲットをロックしたかどうか
    private Vector3 direction;//ユニティちゃんの移動方向
    private Vector3 velocity;//ユニティちゃんの実際の移動距離
    public GameObject target;//ユニティちゃんの移動ターゲット
    private float moveSpeed = 3f;//キャラクターの移動スピードの値

    //障害物判定に必要
    Vector3 playerPos;//目標とするプレイヤーの位置
    float distance;//プレイヤーとの距離


    //状態管理およびアニメーション等
    EnemyStatus enemyStatus;//自身のステータス
    Animator animator;
    EnemySelecter enemySelecter;


    //キャラコン移動に使用
    private CharacterController characterController;



    /*//障害物が間にあるかを判定するレイヤー
    private RaycastHit[] _raycastHits = new RaycastHit[10];
    [SerializeField] private LayerMask raycastLayerMask;
    */


    void Start()
    {
        enemyStatus = GetComponent<EnemyStatus>();
        animator = GetComponent<Animator>();
        enemySelecter = GetComponent<EnemySelecter>();
        characterController = GetComponent<CharacterController>();
        
    }


    private void Update()
    {
        if (!enemyStatus.IsNormal) return;
        if (targetLock == true)
        {
            //移動の値を毎度リセット
            velocity = Vector3.zero;
            //方向を最適化
            direction = (target.transform.position - transform.position).normalized;
            //方向を向かせる
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
            //移動の値にスピードを乗算し代入
            velocity = direction * moveSpeed;

            //もしプレイヤーのすぐ近くまで接近したら
            if (Vector3.Distance(transform.position, target.transform.position) < 1.0f)
            {
                velocity.x = 0;
                velocity.z = 0;
                animator.SetFloat("RunSpeed", 0.0f);
                
                //攻撃のコマンドをオンにする
                if (enemySelecter.activeCommand == false)
                {
                    Debug.Log("きてるよ～");
                    enemySelecter.SetActive();
                }
            }
            //離れている場合はアクティブをオフにし移動させる
            else
            {
                //Speedをアニメーターに反映
                animator.SetFloat("RunSpeed", 2.0f);
                Debug.Log("はずれたよ");
                //攻撃のコマンドをオフにする
                enemySelecter.ResetActive();
            }
            characterController.Move(velocity * Time.deltaTime);
        }
       
    }



    public void OnDetectObject(Collider collider)
    {
        targetLock = true;
        /*var positionDiff = collider.transform.position - transform.position;//自身とプレイヤーの差分を計算
        var distance = positionDiff.magnitude;//プレイヤーとの距離を計算
        var direction = positionDiff.normalized;//プレイヤーの方向
                                                //_raycastHitsにヒットしたコライダーの座標情報などが格納される

        var hitCount = Physics.RaycastNonAlloc(transform.position, direction, _raycastHits, distance, raycastLayerMask);
        Debug.Log("hitcount" + hitCount);

        //間に障害物が無かった場合のみキャラコンで移動させる　その後、距離が近づけば攻撃もさせる
        if (hitCount == 0)
        {*/

        //}
    }

    public void OnOutObject(Collider collider)
    {
        targetLock = false;
        animator.SetFloat("RunSpeed", 0.0f);
    }





}
