﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MobStatus : MonoBehaviour
{
#pragma warning disable 0649
    protected enum StateEnum
    {
        Normal,//ノーマル状態
        Run,//走っている状態
        GoAttack,//ダッシュ攻撃（初撃。ロックオンしてその場へ移動）
        Attack,//攻撃中(コンボ受付中)
        Input,//攻撃中（コンボ受付終了）
        SpAttack,//スペシャルアタック中
        Guard,//ガード中
        Damage,//ダメージのけぞり中
        CoolDown,//ｸｰﾙﾀﾞｳﾝ中
        Die//死んでいる状態
    }

    protected StateEnum state = StateEnum.Normal;

    //状態の取得
    public bool IsNormal => StateEnum.Normal == state;
    public bool IsRun => StateEnum.Run == state;
    public bool IsGoAttack => StateEnum.GoAttack == state;
    public bool IsAttack => StateEnum.Attack == state;
    public bool IsInput => StateEnum.Input == state;
    public bool IsSpAttack => StateEnum.SpAttack == state;
    public bool IsGuard => StateEnum.Guard == state;
    public bool IsDamage => StateEnum.Damage == state;
    public bool IsDie => StateEnum.Die == state;
    public bool IsCoolDown => StateEnum.CoolDown == state;



    protected Animator animator;

    public float level;
    public string charaName;




    //HP。０で死亡
    protected float maxHp = 1200f;
    public virtual float MaxHp => maxHp;
    protected float hp;
    public virtual float Hp => hp;

    //MP。魔法攻撃で消費する
    protected float maxMp = 500f;
    public virtual float MaxMp => maxMp;
    protected float mp;
    public virtual float Mp => mp;

    //SP。無双乱舞で消費する
    protected float maxSp = 500f;
    public virtual float MaxSp => maxSp;
    protected float sp;
    public virtual float Sp => sp;



    //減少繰り返さない基礎ステータス

    protected float power = 150f;//攻撃力。攻撃の威力に影響
    public virtual float Power => power;
    protected float agile = 1.0f;//攻撃速度
    public virtual float Agile => agile;
    protected float defense = 100f;//防御力。受けるダメージに影響
    public virtual float Defense => defense;

   

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        //LifeGaugeContainer.Instance.Add(this);
    }



    //キャラクターが倒れた時の処理
    protected virtual void OnDie()
    {
        //LifeGaugeContainer.Instance.Remove(this);
    }

    //指定したダメージを受ける
    public virtual void Damage(int damage)
    {

    }




    //攻撃しに行っている状態に変更
    public void StateGoAttackPossible()
    {
        state = StateEnum.GoAttack;
    }

    //次の攻撃を受け付けた状態に変更
    public void StateInputPossible()
    {
        state = StateEnum.Input;
    }

    //ステータスを攻撃の最中に変更
    public void StateAttackPossible()
    {
        state = StateEnum.Attack;
    }

    //ステータスを即時にノーマル状態に変更
    public void StateNormalPossible()
    {
        state = StateEnum.Normal;
    }

    //即時ではなくクールダウン状態での復帰
    public void CoolDown()
    {
        state = StateEnum.CoolDown;
        StartCoroutine("CoolDownCoroutine");
    }
    //1秒後にクールダウン状態解除
    IEnumerator CoolDownCoroutine()
    {
        //1秒停止
        yield return new WaitForSeconds(0.5f);
        state = StateEnum.Normal;

    }


    //ステータスを走っている状態に変更

    public void StateRunningPossible()
    {
        state = StateEnum.Run;
    }

    //ステータスをのけぞりに変更
    public void StateDamagePossible()
    {
        animator.ResetTrigger("Attack");
        state = StateEnum.Damage;
    }

    //ステータスを死んでいる状態に変更
    public void StateDiePossible()
    {
        animator.ResetTrigger("Attack");
        state = StateEnum.Die;
    }





}
