﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStatus : MobStatus
{

    EnemySoundScript soundScript;
    //コマンド状態を管理するスクリプト
    EnemySelecter enemySelecter;
    EnemyAttack enemyAttack;
    private NavMeshAgent _agent;


    //指定したダメージを受ける
    public override void Damage(int damage)
    {

        //もしステータスがDieもしくはSpアタック中なら何もしない
        if (IsDie) return;
        //ダメージを受けたらコライダーは全て消す
        enemyAttack.AttackColliderOff();
        //ライフにダメージを反映
        soundScript.PlayDamageSound(0);
        hp -= damage;
        //死んでいなければ
        if (hp > 0)
        {
            animator.SetTrigger("Damage");//ダメージモーションになる
        }
        //HPが残ってたらここで処理は終了
        if (hp > 0) return;

        //0になっている場合は以下の処理
        soundScript.PlayDestroySound(0);
        //ステータスをDieにする
        state = StateEnum.Die;
        OnDie();
    }


    // Start is called before the first frame update
    protected override void Start()
    {
        //サウンドスクリプトを取得
        soundScript = GetComponent<EnemySoundScript>();
        base.Start();
        maxHp = 2000;
        maxMp = 1000;
        hp = maxHp;
        mp = maxMp;
        power = 50f;
        _agent = GetComponent<NavMeshAgent>();
        enemyAttack=GetComponent<EnemyAttack>();
        enemySelecter = GetComponent<EnemySelecter>();

    }



    //ノーマル状態へ遷移する、敵専用のメソッド
    public void EnemyStateNormalPossible()
    {
        //ノーマル状態へ遷移
        state = StateEnum.Normal;
        enemySelecter.ResetActive();
    }


    public void EnemyDamagePossible()
    {
        //もしアクティブ状態なら攻撃コルーチンを中断させる。
        if (enemySelecter.ActiveCommand == true)
        {
            enemySelecter.AttackNg();
        }
        animator.ResetTrigger("Attack");
        enemyAttack.AttackColliderOff();
        //アクティブ問わず状態をダメージへ
        state = StateEnum.Damage;
    }




    protected override void OnDie()
    {
        animator.SetTrigger("Die");
        this.tag = "EnemyDie";//タグを死んだ状態にする
        this.gameObject.layer =14;//レイヤーをアウトオブジェクトという、完全に切り離したものにする
        //3秒後に消える
        Destroy(this.gameObject,3.0f);

    }

    IEnumerator End()
    {
        //3秒停止
        yield return new WaitForSeconds(3);
    }





}
