﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject targetObject;
    MobStatus status;
    Text statusText;
    public Image HpValue;

    void Start()
    {
        status = targetObject.GetComponent<MobStatus>();
        statusText = GetComponent<Text>();
    }

    void Update()
    {
        statusText.text = status.charaName;
        HpValue.fillAmount = status.Hp / status.MaxHp;

    }
}
