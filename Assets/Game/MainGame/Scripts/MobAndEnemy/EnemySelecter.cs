﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class EnemySelecter : MonoBehaviour
{
#pragma warning disable 0414
#pragma warning disable 0649

    private EnemyStatus enemyStatus;

    public  bool activeCommand;//アクティブ状態か否か。
    public  bool ActiveCommand => activeCommand;//同状態をEnemyStatus側に受け渡しする

    private float time;//攻撃開始までの時間
    private int num;//攻撃回数
    Animator animator;




    private void Start()
    {
        enemyStatus = GetComponent<EnemyStatus>();

        animator = GetComponent<Animator>();
    }





    //もし攻撃前にダメージを受けた場合はこのメソッドでコルーチンを止める
    public void AttackNg()
    {
        StopCoroutine("AttackStart");
    }





    //攻撃状態をセットする
    public void SetActive()
    {
        //ステータスを攻撃移行中へ
        enemyStatus.StateGoAttackPossible();
        activeCommand = true;
        time = Random.Range(0.0f, 0.3f);
        //再度攻撃コルーチンを開始する
        StartCoroutine("AttackStart");
    }

    //攻撃状態を解除する
    public void ResetActive()
    {
        //ステータスを非アクティブへ
        activeCommand = false;
        Debug.Log("状態がリセットされたよ");
    }





   

    //攻撃を始めるコルーチン
    IEnumerator AttackStart()
    {
        //ランダム時間停止させた後に攻撃させる
        yield return new WaitForSeconds(time);
        //攻撃の回数をランダムで決定 1以上5未満、最大4回攻撃
        num = (int)Random.Range(1.0f, 5.0f);
        //攻撃トリガーと攻撃回数をアニメーションへ
        animator.SetTrigger("Attack");
        animator.SetInteger("Num",num);
        Debug.Log("攻撃回数は" + num);
    }




    public float GetNum()
    {
        return num;
    }












}