﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    private EnemyStatus status;
    private int damagePoint;
    //private int ran;//乱数用パラメータ
    Animator animator;
    EnemySelecter enemySelecter;


    EnemySoundScript soundScript;//サウンド

    public Collider attackCollider;
    private float num = 0;


    private void Start()
    {
        soundScript = GetComponent<EnemySoundScript>();
        animator = GetComponentInChildren<Animator>();
        status = GetComponent<EnemyStatus>();
        enemySelecter = GetComponent<EnemySelecter>();

    }


    public void OnHitAttack(Collider collider)
    {
        Debug.Log("ダメージを受けた");
        //ターゲット範囲にいるプレイヤーステータスを取得
        var targetMob = collider.GetComponent<PlayerStatus>();
        //範囲内座標にターゲットモブがいなければ何もしない
        if (null == targetMob) return;
        //ランダム変数を適用し、プレイヤーにダメージを与える
        //ダメージポイントを0.8以上1.2未満の中から乱数作り適用
        damagePoint = (int)(status.Power * (Random.Range(0.8f, 1.2f)));


        //数値をターゲットへ渡す
        targetMob.Damage(damagePoint);
        //当たった音を響かせる
        soundScript.PlayHitSound(0);
    }



    //攻撃の開始時にモーションから呼ばせる
    public void OnAttack1Start()
    {
        soundScript.PlaySwingSound(0);
        damagePoint = (int)(status.Power * 1.0f);
        attackCollider.enabled = true;
    }

    //攻撃の終了時に呼ばれる
    public void OnAttack1Finished()
    {
        attackCollider.enabled = false;
    }

    //攻撃の開始時にモーションから呼ばせる
    public void OnAttack2Start()
    {
        soundScript.PlaySwingSound(0);
        damagePoint = (int)(status.Power * 1.1f);
        attackCollider.enabled = true;
    }

    //攻撃の終了時に呼ばれる
    public void OnAttack2Finished()
    {
        attackCollider.enabled = false;
    }

    //攻撃の開始時にモーションから呼ばせる
    public void OnAttack3Start()
    {
        soundScript.PlaySwingSound(0);
        damagePoint = (int)(status.Power * 1.2f);
        attackCollider.enabled = true;
    }

    //攻撃の終了時に呼ばれる
    public void OnAttack3Finished()
    {
        attackCollider.enabled = false;
    }

    //攻撃の開始時にモーションから呼ばせる
    public void OnAttack4Start()
    {
        soundScript.PlaySwingSound(0);
        damagePoint = (int)(status.Power * 1.3f);
        attackCollider.enabled = true;
    }

    //攻撃の終了時に呼ばれる
    public void OnAttack4Finished()
    {
        attackCollider.enabled = false;
    }

    //ダメージを受けた際などに使う攻撃コライダーオフメソッド
    public void AttackColliderOff()
    {
        OnAttack1Finished();
        OnAttack2Finished();
        OnAttack3Finished();
        OnAttack4Finished();

    }








}