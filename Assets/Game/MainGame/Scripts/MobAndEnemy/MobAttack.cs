﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//攻撃を制御するためのクラス
[RequireComponent(typeof(MobStatus))]
public class MobAttack : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] private Collider attackCollider;//地上攻撃のコライダー

    EnemySoundScript soundScript;
    private MobStatus status;
    protected float attackPower = 10f;
    private int damagePoint = 10;

    protected void Start()
    {
        soundScript = GetComponent<EnemySoundScript>();
        status = GetComponent<MobStatus>();
    }


    //攻撃の開始時に呼ばれる
    public void OnAttackStart()
    {
        soundScript.PlaySwingSound(0);
        attackCollider.enabled = true;
    }

    //攻撃の終了時に呼ばれる
    public void OnAttackFinished()
    {
        attackCollider.enabled = false;
    }

    //attackColliderが対象にHitしたら呼ばれる
    public void OnHitAttack(Collider collider)
    {
        //ターゲット範囲にいるモブステータスを取得
        var targetMob = collider.GetComponent<MobStatus>();

        //範囲内座標にターゲットモブがいなければ何もしない
        if (null == targetMob) return;
        //ランダム変数を適用し、プレイヤーにダメージを与える
        //ダメージポイントを0.8以上1.2未満の中から乱数作り適用
        damagePoint = (int)(status.Power * (Random.Range(0.8f, 1.2f)));
        //できたダメージポイントを表記
        Debug.Log("敵の攻撃がヒット" + damagePoint.ToString());
        //ボボン！という音

        //数値をターゲットへ渡す
        targetMob.Damage(damagePoint);

    }





}