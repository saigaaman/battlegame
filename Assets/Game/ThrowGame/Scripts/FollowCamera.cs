﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FollowCamera : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] CinemachineFreeLook freeLookCamera;



    void Start()
    {

    }

    //現在は後ろを追尾するのみなので、マウス移動による方向移動を全て無効にする。
    void Update()
    {
            freeLookCamera.m_XAxis.m_MaxSpeed = 0;
            freeLookCamera.m_YAxis.m_MaxSpeed = 0;

    }
}
