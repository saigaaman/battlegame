﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ThrowBallManager : MonoBehaviour
{
#pragma warning disable 0649

   
    private float moveSpeed;
    private Rigidbody rb;
    UnityChanController unityChanController;




    void OnEnable()
    {

        
        //Rigidbodyを取得する
        rb = GetComponent<Rigidbody>();

        //プレイヤーの情報を取得
        GameObject player = GameObject.Find("unitychan");
        //Player側のスピードを取得する
        unityChanController = player.GetComponent<UnityChanController>();
        moveSpeed = unityChanController.MoveSpeed;
        Debug.Log(""+ unityChanController.MoveSpeed);

        // カメラの方向から、XYZ平面の単位ベクトルを取得
        Vector3 cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 1, 1)).normalized;

        //カメラの方向に向けてspeed分だけの力をかける
        rb.AddForce(cameraForward * moveSpeed,ForceMode.Impulse);     

    }





}
