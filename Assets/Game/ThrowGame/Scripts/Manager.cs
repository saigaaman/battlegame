﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] Text distance;//Unityちゃんとオーバーラインとの距離
    [SerializeField] Text speed;//Unityちゃんの走るスピード（おおよそ）
    [SerializeField] Text throwText;//投擲するためのテキスト
    [SerializeField] Text score;//スコア表記のテキスト
    [SerializeField] GameObject player;//プレイヤー
    [SerializeField] GameObject finalLine;//オーバーライン
    UnityChanController unityChanController;
    GameObject throwBall;

    private float second;//x秒ごとに処理するための値
    private bool textOn;//投擲テキストがオンか否か。点滅用に使う
    private bool game;//ボールを投げた状態かどうか
    private int gameScore;

    void Start()
    {
        unityChanController = player.GetComponent<UnityChanController>();
        //距離および速度テキストを開始時に取得
        distance.text =((int)((finalLine.transform.position.z-player.transform.position.z)*0.5f)).ToString()+"m";
        speed.text = (unityChanController.MoveSpeed*2.0f).ToString()+"km";
        //スローテキストを透明に
        throwText.color=new Color (156,0,0,0);
        score.color = new Color(255, 0, 224, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //距離および速度テキストを開始時に毎回取得
        distance.text =((int)((finalLine.transform.position.z- player.transform.position.z)*0.5f)).ToString()+"m";
        speed.text = (unityChanController.MoveSpeed * 2.0f).ToString() + "km";

        //もし左クリックを押していたら
        if (Input.GetButton("Fire1"))
        {
            //投擲テキストを点滅させる
            second += Time.deltaTime;//時間をカウント
            if (second >= 0.2f)//もし0.2秒たったら（クリック中は速度を1基準から0.3にしてるので。点滅感覚0.6666秒換算）
            {
                //一秒経過するとカウントした時間をリセットする事で0.2秒ごとの処理
                second = 0;
                if (textOn == false)
                {
                    throwText.color = new Color(156, 0, 0, 255);
                    textOn = true;
                }
                else
                {
                    throwText.color = new Color(156, 0, 0, 0);
                    textOn = false;
                }
            }


        //左クリックを離したら
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            //テキストの色を透明に
            throwText.color = new Color(156, 0, 0, 0);
            textOn = false;
        }

        //もし投擲されていたなら
        if (unityChanController.WellGetThere == true)
        {
            //初回のみ、投げたボールオブジェクトを取得し代入
            if (game == false)
            {
                throwBall=GameObject.Find("ThrowBall(Clone)");
                game = true;
            }

            //スコアに反映させる
            gameScore = (int)((throwBall.transform.position.z - 200 )*0.5f);
            score.text = "スコア："+gameScore.ToString()+"メートル";
            

            //スコアを表記する
            score.color = new Color(255, 0, 224, 255);
        }






    }
}
