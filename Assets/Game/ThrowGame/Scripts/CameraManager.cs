﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraManager : MonoBehaviour
{
#pragma warning disable 0649
    //CameraChangeCoroutine()を制御するための変数
    IEnumerator routine;
    [SerializeField] Camera mainCamera;
    [SerializeField] GameObject player;
    UnityChanController unityChanController;
    [SerializeField] CinemachineFreeLook freeLookCamera;


    private void Start()
    {
        //Unityちゃんの現在ステータスを受け取る
        unityChanController = player.GetComponent<UnityChanController>();
        //CameraChangeCoroutineコルーチンを制御可能にする
        routine = CameraChangeCoroutine();
    }




    void Update()
    {
        //左クリックがされている状態かつ、モデル側の移動が止まっていない
        if (Input.GetButton("Fire1")&& unityChanController.WellGetThere==false)
        {
            //投擲用のFPSカメラにするため、フリールックカメラはfalse
            freeLookCamera.enabled = false;
            //透過のためコルーチン開始
            StartCoroutine("CameraChangeCoroutine");
            //投擲中は時間を遅くする
            Time.timeScale = 0.3f;
        }
        //それ以外なら
        else
        {
            //コルーチンを停止させる
            StopCoroutine("CameraChangeCoroutine");
            //さらに、routineをnullにして再度コルーチンを入れる。これによりコルーチンは続きからでなく1からとなる
            routine = null;
            routine = CameraChangeCoroutine();
            //時間をもとに戻す
            Time.timeScale = 1.0f;

            //CinemaChineでは、enabledになれば次の優先順位のものに動的に遷移していく。
            //即時に切り替えたければ、GameObjectをSetActiveでfalseにすれば瞬間的に切り替わる

            //フリールックカメラをtrueにする
            freeLookCamera.enabled = true;
            //レイヤーを元に戻す
            mainCamera.cullingMask |= (1 << 8);
        }
        
    }

    private IEnumerator CameraChangeCoroutine() 
    {
        //一定時間後にUnityちゃんを透過する
        yield return new WaitForSeconds(0.25f);
        mainCamera.cullingMask &= ~(1 << 8);
        

    }






}
