﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class UnityChanController : MonoBehaviour
{
#pragma warning disable 0649
    CharacterController controller;
    Animator animator;
    //プレイヤーの右手
    [SerializeField] GameObject rightHands;
    //最初のボール
    [SerializeField] GameObject haveBall;
    //投げる際にすり替わるボール
    [SerializeField] GameObject throwBall;



    private Vector3 pos;//ユニティちゃんの初期位置
    private Vector3 movePos;//ユニティちゃんがリアルタイムに移動している位置。
    private Vector3 direction;//ユニティちゃんの移動方向 
    private Vector3 velocity;//ユニティちゃんの実際の移動距離
    private Vector3 target;//ユニティちゃんの移動ターゲット

    private bool wellGetThere;//移動ポイントに到達したか否か。到達したらtrueで停止。
    //プロパティ。
    public bool WellGetThere
    {
        get
        {
            return wellGetThere;
        }
        set
        {
            wellGetThere = value;
        }
    }
    private float moveSpeed = 10.0f;//ユニティちゃんの移動スピードの値。最初は１からどんどん加速

    //moveSpeedのプロパティ。外部アクセス時　参照；MoveSpeed()　上書きMoveSpeed(値)
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }


    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();//ユニティちゃんを初期位置へ配置し、キャラコンをオン

        //wellGetThere = true;
        //↑コルーチン後に走らせる感じにする場合
        this.transform.position = new Vector3(0, 0.01f, 0);
        target = new Vector3(0, 0.01f, 200);
        controller.enabled = true;
        velocity = Vector3.zero;


    }


    void Update()
    {
        if (wellGetThere == false)
        { //モデルが目標地点に到着していなければキャラコン移動を実行         
            if (controller.isGrounded)
            {
                velocity = Vector3.zero;//移動の値を毎度リセット
                animator.SetFloat("Speed",moveSpeed*0.1f);//Speedをアニメーターに反映
                direction = (target - transform.position).normalized;//方向を正規化

                 //方向を向かせる
                transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
                //マウスが左クリックされていなければ
                if (Input.GetButton("Fire1")==false) {
                    //加速させる
                    moveSpeed += 2.0f * Time.deltaTime;
                }
                //もし目的地にほぼ誤差なく到達していたら           
                if (Vector3.Distance(transform.position, target) < 0.2f)
                {
                    wellGetThere = true;
                    //本来はガックリモーション＋スコア０処理
                    animator.SetFloat("Speed", 0.0f);
                }

            }
        }
        else
        {
            //目標地点にまで到達していたらリセット
            moveSpeed = 0;
        }
        velocity = direction * moveSpeed;
        velocity.y += Physics.gravity.y * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if (Input.GetButton("Fire1")&&Input.GetButtonDown("Jump"))
        {
            //ボールを出現させる 出現位置はプレイヤーの右手から放つ
            Instantiate(throwBall, new Vector3(rightHands.transform.position.x-0.25f, rightHands.transform.position.y, rightHands.transform.position.z), Quaternion.identity);

            //ボールを消す
            Destroy(haveBall);

            //投げるモーション（前にステップ的な）＋ボール飛んでいく
            animator.SetFloat("Speed",0.0f);
            //キャラ停止
            wellGetThere = true;
            //カメラ側に処理を投げる
            //マネージャー側にスコアを記録する
            //シーン遷移しつつ二投目へ

        }
        






    }
}
