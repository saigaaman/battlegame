﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FpsCamera : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] CinemachineVirtualCamera _vCam;
    CinemachinePOV pov;

    //POVを最優先に認識させるため、Awakeを使う。これによりOnEnable内POVが開幕も問題無く動く
    void Awake()
    {
        pov = _vCam.GetCinemachineComponent<CinemachinePOV>();

    }

    //enabledがオンになる度に初期値へリセット
    private void OnEnable()
    {
        pov.m_HorizontalAxis.Value = 0;
        pov.m_VerticalAxis.Value = 0;
    }




    //マウスクリック時だけ一定のスピードでスクロールを許す。
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            pov.m_HorizontalAxis.m_MaxSpeed = 300f;
            pov.m_VerticalAxis.m_MaxSpeed = 300f;
        }
        else
        {
            pov.m_HorizontalAxis.m_MaxSpeed = 0f;
            pov.m_VerticalAxis.m_MaxSpeed = 0f;
        }

    }






}
